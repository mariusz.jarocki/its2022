<?php

    require("lib.php");

    header("Content-type: application/json");

    $db = db_connect(false, false);
    $roles = getRoles($db);
    if($roles == null) {
        http_response_code(401);
        die('{"error":"Not authorized"}');
    }
    if(!strstr($roles, "a")) {
        http_response_code(403);
        die('{"error":"Permission denied"}');
    }

    switch($_SERVER["REQUEST_METHOD"]) {
        case "GET":
            $st = $db->prepare("SELECT session.name,user.login,user.roles,session.valid,session.lastseen FROM session " .
                               "LEFT JOIN user ON session.login_id=user.id");
            $query = $st->execute();
            $users = Array();
            while($user = $query->fetchArray(SQLITE3_ASSOC)) {
                array_push($users, $user);
            }
            echo(json_encode($users));
            break;
        default:
            http_response_code(405);
            die('{"error":"Method not implemented"}');
    }
?>