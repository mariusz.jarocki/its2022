<?php

    require("lib.php");

    header("Content-type: application/json");

    $db = db_connect();
    $roles = getRoles($db);
    if($roles == null) {
        http_response_code(401);
        die('{"error":"Not authorized"}');
    }
    if(!strstr($roles, "i")) {
        http_response_code(403);
        die('{"error":"Permission denied"}');
    }

    function invoice_validate($invoice) {
        return $invoice->no && is_array($invoice->list) && count($invoice->list) > 0;
    }

    function invoice_get($no) {
        global $db;
        $st = $db->prepare("SELECT * FROM invoice WHERE no=:no");
        $st->bindValue(':no', $no, SQLITE3_TEXT);
        $query = $st->execute();
        $invoice = $query->fetchArray(SQLITE3_ASSOC);
        if(!$invoice) return null;
        $invoice["list"] = Array();
        $st = $db->prepare("SELECT * FROM invoice_list WHERE invoice_id=:invoice_id");
        $st->bindValue(":invoice_id", $invoice["id"], SQLITE3_INTEGER);
        $query = $st->execute();
        while($row = $query->fetchArray(SQLITE3_ASSOC)) {
            array_push($invoice["list"], $row);
        }        
        return $invoice;
    }

    function invoice_getAll($selector) {
        global $db;
        $queryStr = <<< EOQ
            SELECT * FROM (SELECT ROW_NUMBER() OVER (ORDER BY no) row,* FROM (SELECT
            invoice.no,invoice.client,invoice.status,
            COUNT(invoice_list.id) AS rows,
            COALESCE(SUM(invoice_list.price*invoice_list.quantity*(1+invoice_list.vat*0.01)),0) AS value 
            FROM invoice 
            LEFT JOIN invoice_list ON invoice_list.invoice_id=invoice.id 
            GROUP BY invoice.no
            ) q WHERE q.no LIKE :selector_no AND q.client LIKE :selector_client
            AND q.value>=:selector_min AND q.value<=:selector_max
            ORDER BY q.no) qq WHERE qq.row>=:selector_from AND qq.row<=:selector_to
        EOQ;
        if(!property_exists($selector, 'no')) $selector->no = '';
        if(!property_exists($selector, 'client')) $selector->client = '';
        if(!property_exists($selector, 'minValue') || $selector->minValue <= 0) $selector->minValue = 0;
        if(!property_exists($selector, 'maxValue') || $selector->maxValue <= 0) $selector->maxValue = 9999999;
        if(!property_exists($selector, 'from') || $selector->from <= 0) $selector->from = 1;
        if(!property_exists($selector, 'limit') || $selector->limit <= 0) $selector->limit = 10;
        $st = $db->prepare($queryStr);
        $st->bindValue(":selector_no", "%" . $selector->no . "%", SQLITE3_TEXT);
        $st->bindValue(":selector_client", "%" . $selector->client . "%", SQLITE3_TEXT);
        $st->bindValue(":selector_min", $selector->minValue, SQLITE3_FLOAT);
        $st->bindValue(":selector_max", $selector->maxValue, SQLITE3_FLOAT);
        $st->bindValue(":selector_from", $selector->from, SQLITE3_INTEGER);
        $st->bindValue(":selector_to", $selector->from + $selector->limit - 1, SQLITE3_INTEGER);
        $query= $st->execute();
        $rows = Array();
        while($row = $query->fetchArray(SQLITE3_ASSOC)) {
            array_push($rows, $row);
        }        
        return $rows;
    }

    function invoice_getCount() {
        global $db;
        $st = $db->prepare("SELECT COUNT(*) AS invoices_count FROM invoice");
        $query = $st->execute();
        $result = $query->fetchArray(SQLITE3_ASSOC);
        return $result["invoices_count"];
    }

    function invoice_getCountFiltered($selector) {
        global $db;
        $queryStr = <<< EOQ
            SELECT COUNT(*) AS invoices_count_filtered FROM (SELECT no,client,
            COALESCE(SUM(invoice_list.price*invoice_list.quantity*(1+invoice_list.vat*0.01)),0) AS value
            FROM invoice 
            LEFT JOIN invoice_list ON invoice_list.invoice_id=invoice.id 
            GROUP BY invoice.no
            ) q WHERE q.no LIKE :selector_no AND q.client LIKE :selector_client
            AND q.value>=:selector_min AND q.value<=:selector_max
        EOQ;
        if($selector->minValue <= 0) $selector->minValue = 0;
        if($selector->maxValue <= 0) $selector->maxValue = 9999999;
        $st = $db->prepare($queryStr);
        $st->bindValue(":selector_no", "%" . $selector->no . "%", SQLITE3_TEXT);
        $st->bindValue(":selector_client", "%" . $selector->client . "%", SQLITE3_TEXT);
        $st->bindValue(":selector_min", $selector->minValue, SQLITE3_FLOAT);
        $st->bindValue(":selector_max", $selector->maxValue, SQLITE3_FLOAT);
        $query= $st->execute();
        $row = $query->fetchArray(SQLITE3_ASSOC);
        return $row["invoices_count_filtered"];
    }

    function invoice_delete($no) {
        global $db;
        $invoice = invoice_get($no);
        if(!$invoice) return;
        $st = $db->prepare("DELETE FROM invoice_list WHERE invoice_id=:id");
        $st->bindValue(':id', $invoice["id"], SQLITE3_INTEGER);
        $query = $st->execute();
        $st = $db->prepare("DELETE FROM invoice WHERE id=:id");
        $st->bindValue(':id', $invoice["id"], SQLITE3_INTEGER);
        $query = $st->execute();
    }

    function invoice_save($invoice) {
        global $db;
        $st = $db->prepare("INSERT INTO invoice (no,client,status) VALUES (:no,:client,:status)");
        $st->bindValue(':no', $invoice->no, SQLITE3_TEXT);
        $st->bindValue(':client', $invoice->client, SQLITE3_TEXT);
        $st->bindValue(':status', $invoice->status, SQLITE3_TEXT);
        $st->execute();
        $invoice_id = $db->lastInsertRowID();
        foreach($invoice->list as $row) {
            $st = $db->prepare("INSERT INTO invoice_list (invoice_id,name,price,quantity,vat)" .
                               "VALUES (:invoice_id,:name,:price,:quantity,:vat)");
            $st->bindValue(':invoice_id', $invoice_id, SQLITE3_INTEGER);
            $st->bindValue(':name', $row->name, SQLITE3_TEXT);
            $st->bindValue(':price', $row->price, SQLITE3_FLOAT);
            $st->bindValue(':quantity', $row->quantity, SQLITE3_FLOAT);
            $st->bindValue(':vat', $row->vat, SQLITE3_FLOAT);
            $st->execute();
        }
    }

    switch($_SERVER["REQUEST_METHOD"]) {
        case "GET":
            $no = @$_GET["no"];
            if($no) {
                $output = invoice_get($no);
                if(!$output) {
                    http_response_code(404);
                    die('{"error":"No such invoice"}');
                    break;
                }
            } else {
                $selector = @json_decode($_GET["selector"]);
                if(is_null($selector)) $selector = new stdClass();
                $output = new stdClass();
                if(@$_GET["count"] != "") {
                    $output->invoices = invoice_getCount();
                    $output->invoices_filtered = invoice_getCountFiltered($selector);
                } else {
                    $output = invoice_getAll($selector);
                }
            }
            echo(json_encode($output));
            break;

        case "POST":
            $request_body = file_get_contents('php://input');
            $payload = json_decode($request_body);
            if($payload == null) {
                http_response_code(400);
                die('{"error":"Invalid payload"}');
            }
            if(!invoice_validate($payload)) {
                http_response_code(400);
                die('{"error":"Invalid invoice"}');
            }
            $saved = invoice_get($payload->no);
            if($saved && $saved["status"] == 1) {
                http_response_code(403);
                die('{"error":"Booked invoice"}');
            }
            if($saved) {
                invoice_delete($payload->no);
            }
            invoice_save($payload);
            echo(json_encode($payload));
            break;

        case "DELETE":
            $no = $_GET["no"];
            if($no) {
                $saved = invoice_get($no);
                if(!$saved) {
                    http_response_code(404);
                    die('{"error":"No such invoice"}');
                    break;
                }
                if($saved && $saved["status"] == 1) {
                    http_response_code(403);
                    die('{"error":"Booked invoice"}');
                    break;
                }
                invoice_delete($no);
            } else {
                http_response_code(403);
                die('{"error":"No invoice to delete"}');
                break;
            }
            echo(json_encode($saved));
            break;

        default:
            http_response_code(405);
            die('{"error":"Method not implemented"}');
    }
?>