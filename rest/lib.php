<?php

$wsUrl = 'ws://127.0.0.1/its2022/ws/broadcast';

require __DIR__ . '/../vendor/autoload.php';

function db_connect($sendTouch = true, $updateLastSeen = true) {
    global $wsUrl;
    $db = new SQLite3("../db/its2022.sqlite3");
    if(!$db) {
        http_response_code(500);
        die('{"error":"Connection to database failed"}');
    }
    $db->busyTimeout = 5000;
    if(@$_COOKIE["session"]) {

        if($updateLastSeen) {
            $st = $db->prepare("UPDATE session SET lastseen=:now WHERE name=:name");
            $st->bindValue(':now', time(), SQLITE3_INTEGER);
            $st->bindValue(':name', $_COOKIE["session"], SQLITE3_TEXT);
            $st->execute();
        }

        // wyślij informację do serwera ws
        if($sendTouch) {
            $client = new WebSocket\Client($wsUrl);
            $client->text('{"event":"touch","session":"' . $_COOKIE["session"] . '"}');
        }
    }
    return $db;
}

function getRoles($db) {
    $st = $db->prepare("SELECT user.login,user.roles FROM session " .
                       "LEFT JOIN user ON session.login_id=user.id " .
                       "WHERE session.name=:name");
    $st->bindValue(':name', $_COOKIE["session"], SQLITE3_TEXT);
    $query = $st->execute();
    $user = $query->fetchArray(SQLITE3_ASSOC);
    return !$user || !$user["login"] ? null : $user["roles"];
}

?>