<?php

    require("lib.php");

    $auth_scope = "/its2022";
    $timeToLive = 24 * 60 * 60; // 1 dzień

    header("Content-type: application/json");

    $db = db_connect();

    switch($_SERVER["REQUEST_METHOD"]) {
        case "GET":
            $loggedUser = new stdClass();
            if(@!empty($_COOKIE["session"])) {
                $st = $db->prepare("SELECT session.name AS session,user.login,user.roles,session.valid,session.lastseen FROM session " .
                                "LEFT JOIN user ON session.login_id=user.id " .
                                "WHERE session.name=:name");
                $st->bindValue(':name', $_COOKIE["session"], SQLITE3_TEXT);
                $query = $st->execute();
                $user = $query->fetchArray(SQLITE3_ASSOC);
                $loggedUser->session = @$user["session"];
                $loggedUser->login = @$user["login"];
                $loggedUser->roles = @$user["roles"];
                $loggedUser->valid = @$user["valid"];
                $loggedUser->lastseen = @$user["lastseen"];
            }
            echo(json_encode($loggedUser));
            break;

        case "POST":
            $request_body = file_get_contents('php://input');
            $payload = json_decode($request_body);
            if($payload == null || @!$payload->login || @!$payload->password) {
                http_response_code(400);
                die('{"error":"Invalid payload"}');
            }
            $st = $db->prepare("SELECT * FROM user WHERE login=:login");
            $st->bindValue(':login', $payload->login, SQLITE3_TEXT);
            $query = $st->execute();
            $user = $query->fetchArray(SQLITE3_ASSOC);
            if(!$user || $user["password"] != hash("sha256", $payload->password)) {
                http_response_code(401);
                die('{"error":"Authorization failed"}');
            }

            // czyszczenie przeterminowanych sesji
            $st = $db->prepare("DELETE FROM session WHERE valid<:now");
            $st->bindValue(':now', time(), SQLITE3_INTEGER);
            $query = $st->execute();

            $loginStatus = new stdClass();
            $loginStatus->status = "OK";
            $loginStatus->login = $user["login"];
            $loginStatus->roles = $user["roles"];

            // nazwa sesji to uuid v4
            $session = vsprintf('%s%s-%s-%s-%s-%s%s%s', str_split(bin2hex(random_bytes(16)), 4));

            $loginStatus->session = $session;
            $loginStatus->lastseen = time();
            $loginStatus->valid = $loginStatus->lastseen + $timeToLive;
            setcookie("session", $session, $loginStatus->valid, $auth_scope);
            $st = $db->prepare("DELETE FROM session WHERE name=:name");
            $st->bindValue(':name', $session, SQLITE3_TEXT);
            $query = $st->execute();
            $st = $db->prepare("INSERT INTO session (name,login_id,valid) VALUES (:name,:login_id,:valid)");
            $st->bindValue(':name', $session, SQLITE3_TEXT);
            $st->bindValue(':login_id', $user["id"], SQLITE3_INTEGER);
            $st->bindValue(':valid', $loginStatus->valid, SQLITE3_INTEGER);
            $st->bindValue(':lastseen', $loginStatus->lastseen, SQLITE3_INTEGER);
            $query = $st->execute();
            echo(json_encode($loginStatus));
            break;

        case "DELETE":
            $st = $db->prepare("DELETE FROM session WHERE name=:name");
            $st->bindValue(':name', $_COOKIE["session"], SQLITE3_TEXT);
            setcookie("session", "", time() - 3600, $auth_scope);
            $query = $st->execute();
            $deletedStatus = new stdClass();
            $deletedStatus->status = 'OK';
            echo(json_encode($deletedStatus));
            break;

        default:
            http_response_code(405);
            die('{"error":"Method not implemented"}');
    }
?>