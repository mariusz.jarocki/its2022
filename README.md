# Inicjalizacja projektu

## Przygotowanie bazy danych
`
sqlite3 db/its2022.sqlite3 < db/init.sql
chmod a+w db
chmod a+w db/its2022.sqlite3
`

## Dodatkowe oprogramowanie
`
npm install
composer install
`

## Konfiguracja apache

    ProxyPassMatch ^/its2022/ws/(.*) ws://localhost:8880/$1
    ProxyPassReverse ^/its2022/ws/(.*) ws://localhost:8880/$1

    <Directory /home/jarocki/public_html>
        AllowOverride all
    </Directory>

## Start serwera ws
`
cd ws-server
php ws-server.php
`

# Zadania zaliczeniowe

## Na ocenę 3

* Stworzyć nowego użytkownika "control" i nową rolę "c"
* Użytkownicy w roli "c" mają dostęp tylko do nowego widoku audit.html
* Widok ten przedstawia podatek VAT należny w poszczególnych stawkach podatkowych, czyli price\*quantity\*(vat/100) np. 22% - 154.34zł, 8% - 78.91zł, 5% - 62.50zł, 0% - 0zł (zapytanie dostępne w pliku db/zestawienie_vat.sql)

## Na ocenę 4

Widok audit.html wyposażyć w dodatkowe kontrolki:
* Można zdefiniować zakres dat faktur (to implikuje dodatkowy atrybut faktury - datę sprzedaży, może być swobodnie ustawiana)
* Można zdefiniować podciąg który musi występować w nazwie kupującego
* W widoku documents.html stworzyć dodatkową kolumnę z datą sprzedaży - po tej wartości można filtrować

## Na ocenę 5

Widok audit.html oraz documents.html powinny automatycznie odświeżać się przy zmianach danych w obszarze tabel invoice i invoice_list