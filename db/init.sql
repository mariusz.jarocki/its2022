drop table if exists user;
create table user (
    id integer primary key autoincrement,
    login text,
    password text,
    roles text
);
create unique index u_login on user (login);
insert into user (login,password,roles) values ('admin','25f43b1486ad95a1398e3eeb3d83bc4010015fcc9bedb35b432e00298d5021f7','aig');
insert into user (login,password,roles) values ('user','0a041b9462caa4a31bac3567e0b6e6fd9100787db2ab433d96f6d178cabfce90','ig');
insert into user (login,password,roles) values ('guest','4676424d5f805a7579abd1236287be2abf24f39b8a622ef587edd7d91b8e2952','g');

drop table if exists session;
create table session (
    name text primary key,
    login_id integer,
    valid integer,
    lastseen integer
);

drop table if exists invoice;
create table invoice (
    id integer primary key autoincrement,
    no text,
    client text,
    status integer
);
create unique index u_no on invoice (no);

drop table if exists invoice_list;
create table invoice_list (
    id integer primary key autoincrement,
    invoice_id integer,
    name text,
    price real,
    quantity real,
    vat real
);