SELECT 
    invoice_list.vat,
    COALESCE(SUM(invoice_list.price*invoice_list.quantity*(invoice_list.vat*0.01)),0) AS vat_value 
    FROM invoice 
    LEFT JOIN invoice_list ON invoice_list.invoice_id=invoice.id 
    GROUP BY invoice_list.vat
    HAVING invoice_list.vat IS NOT NULL