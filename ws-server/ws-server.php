<?php

    use Ratchet\MessageComponentInterface;
    use Ratchet\ConnectionInterface;

    require __DIR__ . '/../vendor/autoload.php';

    $db = new SQLite3("../db/its2022.sqlite3");
    if(!$db) {
        die('WS-Server cannot start, connection to database failed');
    }
    $db->busyTimeout(5000);

    class WS implements MessageComponentInterface {
        protected $clients;

        public function __construct() {
            $this->clients = new \SplObjectStorage;
        }

        public function onOpen(ConnectionInterface $conn) {
            $this->clients->attach($conn);
        }

        public function onMessage(ConnectionInterface $from, $msg) {
            global $db;

            // diagnostyka
            echo($msg . "\n");

            $payload = @json_decode($msg);
            if(@!$payload || @!$payload->event || @!$payload->session) {
                return; // nie przetwarzam innych wiadomości tylko JSON object z polem event
            }

            // rejestracja sesji w serwerze WebSockets
            if($payload->event == 'register') {
                $from->session = $payload->session;
                return;
            }

            // sesje z bazy danych
            $sessions = Array();
            $st = $db->prepare("SELECT session.name,user.login,user.roles FROM session " .
                               "LEFT JOIN user ON session.login_id=user.id ");
            $query = $st->execute();
            while($session = $query->fetchArray(SQLITE3_ASSOC)) {
                $session_data = new stdClass();
                $session_data->login = $session["login"];
                $session_data->roles = $session["roles"];
                $sessions[$session["name"]] = $session_data;
            }
            switch($payload->event) {
                case 'touch':
                case 'login':
                    foreach ($this->clients as $client) {
                        if($from != $client) {
                            $roles = @$sessions[$client->session]->roles;
                            if(!empty($roles) && strstr($roles, "i")) {
                                $client->send($msg);
                            }
                        }
                    }
                    break;
            }
        }

        public function onClose(ConnectionInterface $conn) {
            $this->clients->detach($conn);
        }

        public function onError(ConnectionInterface $conn, \Exception $e) {
            $conn->close();
        }
    }

    $app = new Ratchet\App('localhost', 8880);
    $app->route('/broadcast', new WS, array('*'));
    $app->run();

?>