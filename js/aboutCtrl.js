app.controller('AboutCtrl', [ '$scope', '$http', 'common', function($scope, $http, common) {
    console.log('Kontroler AboutCtrl startuje')
    let ctrl = this

    ctrl.sessions = []
    ctrl.user = common.user
    
    ctrl.refresh = function() {
        $http.get('rest/about.php').then(function(res) {
            ctrl.sessions = res.data
        },function(err) {})
    }

    $scope.$on('touch', function(event, data) {
        ctrl.refresh()
    })

    ctrl.refresh()
}])