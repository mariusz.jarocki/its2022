app.controller('DocumentsCtrl', [ '$location', '$window', '$http', 'common', function($location, $window, $http, common) {
    console.log('Kontroler DocumentsCtrl startuje')
    let ctrl = this

    ctrl.invoices = []
    ctrl.count = ctrl.count_filtered = 0

    ctrl.focused = -1
    ctrl.common = common

    ctrl.getDocuments = function(gotoFirst = true) {
        if(gotoFirst) {
            ctrl.common.selector.from = 1
        }
        /* pobierz liczności */
        $http.get('rest/invoice.php?count=true&selector=' + JSON.stringify(ctrl.common.selector)).then(
            function(res) {
                ctrl.count = res.data.invoices
                ctrl.count_filtered = res.data.invoices_filtered                        
                /* pobierz dane */
                $http.get('rest/invoice.php?selector=' + JSON.stringify(ctrl.common.selector)).then(
                    function(res) {
                    ctrl.invoices = res.data
                },
                function(err) {}
            )},
            function(err) {}
        )
    }

    ctrl.getDocuments()

    ctrl.goToInvoice = function() {
        common.focusedNo = ctrl.invoices[ctrl.focused].no
        $location.path('/invoice')
    }

    ctrl.clearSelector = function() {
        ctrl.common.selector.no = ctrl.common.selector.client = ''
        ctrl.common.selector.minValue = ctrl.common.selector.maxValue = null
        ctrl.getDocuments()
    }

    ctrl.prevDocuments = function() {
        ctrl.common.selector.from -= ctrl.common.selector.limit
        ctrl.getDocuments(false)
    }

    ctrl.nextDocuments = function() {
        ctrl.common.selector.from += ctrl.common.selector.limit
        ctrl.getDocuments(false)
    }

    angular.element($window).on("scroll", function() {
        let windowHeight = "innerHeight" in window ? window.innerHeight : document.documentElement.offsetHeight
        let body = document.body, html = document.documentElement
        let docHeight = Math.max(body.scrollHeight, body.offsetHeight, html.clientHeight, html.scrollHeight, html.offsetHeight)
        windowBottom = windowHeight + window.pageYOffset
        if(windowBottom >= docHeight) {
            ctrl.common.selector.limit += 10
            if(ctrl.common.selector.limit > ctrl.count_filtered) {
                ctrl.common.selector.limit = ctrl.count_filtered
            }
            ctrl.getDocuments()
        }
    })

}])