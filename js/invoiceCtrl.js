app.controller('InvoiceCtrl', [ '$http', 'common', function($http, common) {
    console.log('Kontroler InvoiceCtrl startuje')
    let ctrl = this

    ctrl.vats = [
        { label: '23%', percent: 23 },
        { label: '8%', percent:  8 },
        { label: '5%', percent: 5 },
        { label: '0%', percent: 0 }
    ]

    ctrl.invoices = []

    ctrl.load = function() {
        $http.get('rest/invoice.php?no=' + ctrl.invoice.no).then(
            function(res) {
                ctrl.invoice = res.data
            },
            function(err) {
                console.error(err.data.error)
            }
        )            
    }

    const emptyInvoice = { no: '', client: '', status: 0, list: [] }
    ctrl.invoice = {}
    Object.assign(ctrl.invoice, emptyInvoice)

    const emptyRow = { name: '', price: 0, quantity: 1, vat: ctrl.vats[0].percent }
    ctrl.newRow = {}
    Object.assign(ctrl.newRow, emptyRow)

    ctrl.editedRow = {}
    ctrl.editState = false
    ctrl.editNewState = false

    ctrl.getValue = function(row) {
        if(isNaN(row.price) || isNaN(row.quantity) || isNaN(row.vat))
            return 0
        else
            return row.price * row.quantity * (1 + row.vat * 0.01)
    }

    ctrl.getInvoiceValue = function() {
        let total = 0
        ctrl.invoice.list.forEach(function(row) {
            if(!row.edited)
                total += ctrl.getValue(row)
            else
                total += ctrl.getValue(ctrl.editedRow)
        })
        return total + (ctrl.editNewState ? ctrl.getValue(ctrl.newRow) : 0)
    }

    ctrl.addNewRow = function() {
        let newRowCopy = {}
        Object.assign(newRowCopy, ctrl.newRow)
        ctrl.invoice.list.push(newRowCopy)
        Object.assign(ctrl.newRow, emptyRow)
        ctrl.editNewState = false
    }

    ctrl.delRow = function(index) {
        ctrl.invoice.list.splice(index, 1)
    }

    ctrl.startEditingRow = function(index) {
        Object.assign(ctrl.editedRow, ctrl.invoice.list[index])
        ctrl.invoice.list[index].edited = ctrl.editState = true
    }

    ctrl.endEditingRow = function(index) {
        Object.assign(ctrl.invoice.list[index], ctrl.editedRow)
        delete ctrl.invoice.list[index].edited
        ctrl.editState = false
    }

    ctrl.cancelEditingRow = function(index) {
        delete ctrl.invoice.list[index].edited
        ctrl.editState = false
    }

    let getInvoices = function() {
        $http.get('rest/invoice.php').then(
            function(res) {
                ctrl.invoices = res.data
                if(common.focusedNo) {
                    ctrl.invoice.no = common.focusedNo
                    ctrl.load()
                }
            },
            function(err) {
                console.error(err.data.error)
            }
        )
    }

    ctrl.save = function() {
        let no = ctrl.invoice.no
        $http.post('rest/invoice.php', ctrl.invoice).then(
            function(res) {
                alert('Faktura ' + res.data.no + ' ' + (res.data.status == 1 ? 'zaksięgowana' : 'zapisana'))
                if(res.data.status == 1) {
                    Object.assign(ctrl.invoice, emptyInvoice)
                }
                getInvoices()
            },
            function(err) {
                console.error(err.data.error)
                switch(err.data.error) {
                    case 'Booked invoice': alert('Faktura ' + no + ' zaksięgowana, zapis niemożliwy'); break;
                }
            }
        )
    }

    ctrl.zero = function() {
        ctrl.invoice.list.length = 0
    }

    ctrl.delete = function() {
        $http.delete('rest/invoice.php?no=' + ctrl.invoice.no).then(
            function(res) {
                Object.assign(ctrl.invoice, emptyInvoice)
                getInvoices()
            },
            function(err) {
                console.error(err.data.error)
                switch(err.data.error) {
                    case 'Booked invoice': alert('Faktura ' + ctrl.invoice.no + ' zaksięgowana, usunięcie niemożliwe'); break;
                }
            }
        )            
    }

    getInvoices()

}])