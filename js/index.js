let app = angular.module('its2022', [ 'ngRoute', 'ngSanitize', 'ngAnimate', 'ui.bootstrap', 'ws' ])

const wsProtocol = 'ws'
const wsUrl = '/its2022/ws/broadcast'

app.constant('routes', [
    { route: '/', templateUrl: 'home.html', controller: 'HomeCtrl', controllerAs: 'ctrl', menu: '<i class="fa fa-lg fa-home"></i>', roles: '' },
    { route: '/documents', templateUrl: 'documents.html', controller: 'DocumentsCtrl', controllerAs: 'ctrl', menu: 'Dokumenty', roles: 'ai' },
    { route: '/invoice', templateUrl: 'invoice.html', controller: 'InvoiceCtrl', controllerAs: 'ctrl', menu: 'Faktura', roles: 'ai' },
    { route: '/about', templateUrl: 'about.html', controller: 'AboutCtrl', controllerAs: 'ctrl', menu: 'O systemie', roles: 'a' }
])

// router installation
app.config(['$routeProvider', '$locationProvider', 'routes', function($routeProvider, $locationProvider, routes) {
    $locationProvider.hashPrefix('')
	for(var i in routes) {
		$routeProvider.when(routes[i].route, routes[i])
	}
	$routeProvider.otherwise({ redirectTo: '/' })
}])

// ws installation
app.config(['wsProvider', function(wsProvider) {
    wsProvider.setUrl(wsProtocol + '://' + window.location.host + wsUrl)
}])

app.service('common', [ function() {
    let common = this

    common.user = { login: '', roles: '', session: '' }
    common.focusedNo = ''
    common.selector = { no: '', client: '', minValue: null, maxValue: null, from: 1, limit: 10 }

    let alert = { text: '', type: 'alert-success' }

    common.alertText = function() { return alert.text }
    common.alertType = function() { return alert.type }
    common.alertClose = function() { alert.text = '' }
    common.alertShow = function(text, type = 'success') {
        alert.text = text
        alert.type = 'alert-' + type
        console.log(alert.type + ':', alert.text)
    }

}])

app.controller('Ctrl', [ '$rootScope', '$scope', '$location', '$http', 'ws', 'routes', 'common', function($rootScope, $scope, $location, $http, ws, routes, common) {
    console.log('Kontroler Ctrl startuje')
    let ctrl = this

    ctrl.common = common
    ctrl.user = common.user
    ctrl.creds = { login: '', password: '' }

    ctrl.doLogin = function() {
        $http.post('rest/auth.php', ctrl.creds).then(
            function(rep) {
                ctrl.user.login = rep.data.login
                ctrl.user.roles = rep.data.roles
                ctrl.user.session = rep.data.session
                ws.send(JSON.stringify({
                    event: 'register',
                    session: ctrl.user.session
                }))                        
                rebuildMenu()
                ws.send(JSON.stringify({
                    session: ctrl.user.session,
                    event: 'login',
                    parameter: ctrl.user.login
                }))
            },
            function(err) {
                alert('Logowanie nieudane')
            }
        )
    }

    ctrl.doLogout = function() {
        $http.delete('rest/auth.php').then(function(rep) {
            ctrl.user.login = ctrl.user.roles = ctrl.user.session = ''
            rebuildMenu()
        }, function(err) {})
    }

    // menu building

    ctrl.menu = []

    let rebuildMenu = function() {
        ctrl.menu.length = 0
        for(let i in routes) {
            // albo route jest dostępny dla wszystkich (null) albo rola jest w roli route
            if(!routes[i].roles || routes[i].roles.match(new RegExp('[' + (ctrl.user.roles ? ctrl.user.roles : '') + ']'))) {
                ctrl.menu.push({ route: routes[i].route, title: routes[i].menu })
            }
        }
        $location.path('/')
    }

    ctrl.isCollapsed = true
    $scope.$on('$routeChangeSuccess', function () {
        ctrl.isCollapsed = true
    })
    
    ctrl.navClass = function(page) {
        return page === $location.path() ? 'active' : ''
    }    

    ws.on('message', function(message) {
        try {
            let message_data = JSON.parse(message.data)
            switch(message_data.event) {
                case 'login':
                    common.alertShow('Użytkownik ' + message_data.parameter + ' zalogował się do systemu')
                    break
                case 'touch':
                    $rootScope.$broadcast('touch', message_data)
                    break
            }
        } catch(ex) {}
    })

    $http.get('rest/auth.php').then(function(rep) {
        ctrl.user.login = rep.data.login
        ctrl.user.roles = rep.data.roles
        ctrl.user.session = rep.data.session
        rebuildMenu()
        ws.send(JSON.stringify({
            event: 'register',
            session: ctrl.user.session
        }))
    }, function(err) {})

}])